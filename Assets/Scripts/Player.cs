using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float force;
    [SerializeField] private int jumpPoint;
    [SerializeField] private float impulceForce;
    private Rigidbody2D body;
    private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;
    // private Vector2 _direction;
    private InputSystem _input;
    private Rigidbody2D rigidBody; 

    private void Awake()
    {
        _input = new InputSystem();

        _input.Player.Shoot.performed += context => ShootTrigger();
        _input.Player.Jump.performed += context => JumpTrigger();
    }

    private void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _input.Enable();
    }

    private void OnDisable()
    {
        _input.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 direction = _input.Player.Move.ReadValue<Vector2>();

        if (direction.x < 0)
            spriteRenderer.flipX = false;
        else if (direction.x > 0)
            spriteRenderer.flipX = true;

        Move(direction);
    }

    /* public void OnMove(InputAction.CallbackContext context)
    {
        _direction = context.ReadValue<Vector2>();
    }

    public void OnShoot(InputAction.CallbackContext context)
    {
        Debug.Log("���-���");
    }

    private void Update()
    {
        Move(_direction);
    } */

    private void Move(Vector2 direction)
    {
        animator.SetFloat("Direction", Mathf.Abs(direction.x));
        float scaledMoveSpeed = _speed * Time.deltaTime;

        Vector3 moveDirection = new Vector3(direction.x, 0);
        transform.position += moveDirection * scaledMoveSpeed;
    }

    private void ShootTrigger()
    {
        animator.SetTrigger("AttackTrigger");

        Debug.Log("���-���");
    }

    private void Shoot()
    {
        if (spriteRenderer.flipX == false && rigidBody != null)
            rigidBody.AddForce(Vector2.left * impulceForce, ForceMode2D.Impulse);
        else if (spriteRenderer.flipX == true && rigidBody != null)
            rigidBody.AddForce(Vector2.right * impulceForce, ForceMode2D.Impulse);
    }

    private void JumpTrigger()
    {
        animator.SetTrigger("JumpTrigger");
    }

    private void Jump()
    {
        BufferJump();
        if (jumpPoint > 0)
        {
            body.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            jumpPoint -= 1;
        }
    }

    private void BufferJump()
    {
        Collider2D[] collider2Ds = Physics2D.OverlapBoxAll(transform.position, new Vector2(0.9f, 2.0f), 1.5f);
        for (int i = 0; i < collider2Ds.Length; i++)
        {
            if (collider2Ds[i].gameObject.CompareTag("Platform"))
            {
                jumpPoint = 2;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() != null && col.gameObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
        {
            rigidBody = col.gameObject.GetComponent<Rigidbody2D>();
        }

        if (col.gameObject.CompareTag("Platform"))
        {
            animator.SetBool("IsGround", true);
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Platform"))
        {
            animator.SetBool("IsGround", false);
        }
    }
}
